"use strict";

import { target } from './target.js';

let main = document.getElementById("main");
let tagLink = document.getElementById("tag-link");

async function asyncCall(url) {
    let response = await fetch(url);
    let data = await response.json();

    return data;
};

asyncCall(`../data/${target}/data/index.json`)
    .then(data => {
        data["index"].forEach(index => {
            let section = document.createElement("section");
            main.classList.add("index");
            section.appendChild(createTag("h2", index));
            data[index].forEach(element => {
                let p = createTag("p", element);
                p.setAttribute("name", element);
                p.addEventListener("click", () => {
                    recursiveCall(index, element);
                }, false);
                section.appendChild(p);
            });
            main.appendChild(section);
        });
    })

/**
 * @param text
 * @return {string}
 */
function linkSniffer(text) {
    let cutText = text;
    let categories = [];
    let array = [];
    let i = 0;
    let index = 0;
    while (index !== -1 && i < 10 && cutText.length > 0) {
        index = cutText.indexOf("_")
        if (index !== -1) {
            array.push(index);
            categories.push(index - 1)
            cutText = cutText.substring(index + 1);
        }
        i++;
    }
    for (let i = 0; i < array.length; i++) {
        if (i !== 0) {
            array[i] = array[i] + array[i - 1] + 1;
        }
    }
    let collection = [];
    array.forEach(index => {
        let word = "";
        let condition = true;
        let x = 1;
        while (condition) {
            let i = text[index + x];
            if (i !== " " && i !== undefined && i !== "," && i !== "." && i !== ")") {
                word += text[index + x];
                x++;
            } else {
                condition = false;
            }
        }
        collection.push(word);
    });
    let y = 0;
    for (let i = 0; i < categories.length; i++) {
        let letter = text[array[i] - 1];
        categories[i] = getCategory(letter);
    }
    collection.forEach(word => {
        text = text.replace(`${categories[y][0]}_${word}`, `<span index="${categories[y]}" element="${word.toLowerCase()}" class="link">${word}</span>`);
        y++;
    });
    return text;
}

function recursiveCall(index, element) {
    asyncCall(`../data/${target}/data/${index}/${element}.json`)
        .then(data => {
            main.innerHTML = "";
            main.classList.remove("index");
            if (index === "character") {
                main.appendChild(createTag("h2", `${data.name} ${data.lastname}`));
                main.appendChild(createTag("h3", data.class));
                main.appendChild(createTag("p", `${data.name} fait partie du peuple ${data.race}, c'est ${genderizer(data.gender)[1]} ${data.class} de ${data.age} ans.`));
                main.appendChild(createTag("p", linkSniffer(data.description)));
                displayManyP(data.past, "Passé");
                displayManyP(data.quest, "Quête");
                displayList(data.power, "Pouvoir");
                displayList(data.stuff, "Équipement");
            } else if (index === "artefact") {
                main.appendChild(createTag("h2", data.name));
                main.appendChild(createTag("p", linkSniffer(data.description)));
                displayList(data.power, "Pouvoir");
            } else if (index === "faction") {
                main.appendChild(createTag("h2", data.name));
                main.appendChild(createTag("p", linkSniffer(data.description)));
            } else if (index === "place") {
                main.appendChild(createTag("h2", data.name));
                main.appendChild(createTag("p", linkSniffer(data.description)));
                addHR();
                main.appendChild(createTag("h3", "Localisation"));
                main.appendChild(createTag("p", linkSniffer(data.location)));
                displayList(data.spot, "Lieux");
            } else if (index === "race") {
                main.appendChild(createTag("h2", data.name));
                main.appendChild(createTag("p", linkSniffer(data.description)));
                displayList(data.home, "Lieux de vie");
            } else if (index === "storyline") {
                main.appendChild(createTag("h2", data.name));
                main.appendChild(createTag("p", linkSniffer(data.description)));
                displayManyP(data.step, "Étapes");
            }
            // Find link and redirection
            let links = document.getElementsByClassName("link");
            for (var i = 0; i < links.length; i++) {
                links[i].addEventListener('click', (e) => {
                    recursiveCall(e.target.getAttribute('index'), e.target.getAttribute('element'));
                }, false);
            }
            // Tag display
            let tagList = document.createElement('ul');
            tagList.setAttribute('class', 'tag');
            data.tag.forEach(element => {
                let tag = createTag('li', element);
                tag.addEventListener('click', () => {
                    displayTag(element);
                }, false);
                tagList.appendChild(tag);
            });
            main.appendChild(tagList);
        })
}

tagLink.addEventListener('click', () => {
    asyncCall(`../data/${target}/data/tag.json`)
        .then(data => {
            main.innerHTML = "";
            main.classList.remove("index");
            main.appendChild(createTag('h1', 'Nuage de tags'));
            let section = document.createElement('section');
            section.setAttribute('class', 'tag-page');
            Object.entries(data).forEach(element => {
                let p = createTag('p', element[0]);
                p.style.fontSize = `${15 + element[1].length * 2}px`;
                p.setAttribute('class', 'link');
                p.addEventListener('click', () => {
                    displayTag(element[0]);
                }, false);
                section.appendChild(p);
            });
            main.appendChild(section);
        })
}, false);

function displayTag(element) {
    asyncCall(`../data/${target}/data/tag.json`)
        .then(data => {
            main.innerHTML = "";
            main.appendChild(createTag('h1', `Tag : ${element}`));
            let tagList = document.createElement('ul');
            data[element].forEach(element => {
                let li = createTag('li', element.substring(2));
                li.setAttribute('index', getCategory(element[0]));
                li.setAttribute('element', element.substring(2));
                li.setAttribute('class', 'link');
                li.addEventListener('click', (e) => {
                    recursiveCall(e.target.getAttribute('index'), e.target.getAttribute('element'));
                }, false);
                tagList.appendChild(li);
            });
            main.appendChild(tagList);
        })
}

function createTag(type, text) {
    let tag = document.createElement(type);
    tag.innerHTML = text;
    return tag;
}

function getCategory(letter) {
    if (letter === 'a') return 'artefact';
    if (letter === 'c') return 'character';
    if (letter === 'f') return 'faction';
    if (letter === 'p') return 'place';
    if (letter === 'r') return 'race';
    if (letter === 's') return 'storyline';
}

function genderizer(gender) {
    if (gender === 'f') return ['elle', 'une'];
    if (gender === 'h') return ['il', 'un'];
}

function displayList(list, name = null) {
    if (list.length !== 0) {
        let section = document.createElement('section');
        let ul = document.createElement('ul');
        section.appendChild(addHR());
        if (name !== null) {
            section.appendChild(createTag("h3", name));
        }
        list.forEach(element => {
            let li = document.createElement('li');
            li.innerHTML = linkSniffer(element);
            ul.appendChild(li);
        });
        section.appendChild(ul);
        main.appendChild(section);
    }
}

function displayManyP(array, name = null) {
    if (array.length !== 0) {
        let section = document.createElement('section');
        section.appendChild(addHR());
        if (name !== null) {
            section.appendChild(createTag("h3", name));
        }
        array.forEach(element => {
            let p = document.createElement('p');
            p.innerHTML = linkSniffer(element);
            section.appendChild(p);
        });
        main.appendChild(section);
    }
}

function addHR() {
    let div = document.createElement('div');
    div.setAttribute('class', 'hr');
    for (let i = 0; i < 3; i++) {
        let span = document.createElement('span')
        span.innerHTML = '&#11048;';
        div.appendChild(span);
    }
    return div;
}