#!/bin/bash

indexFile="index.json";
i=0;
y=0;
x=1;
index="\"index\": ["

# Empty the files content
truncate -s 0 $indexFile

echo "{" >> $indexFile

declare -a folders
for entry in "$PWD"/*; do
    folder="$(basename "$entry" | cut -f1 -d'.')"
    if [ "$folder" != "index" -a "$folder" != "tag" ]
    then
        folders+=("$folder")
        index="${index}\"${folder}\", "
    fi
done

index="${index::-2}]"
echo -e "\t$index," >> $indexFile

for folder in "${folders[@]}"
do
    cd "$folder" || exit
    echo -e "\t\"$folder\": [" >> "../${indexFile}"

    # List length calculation
    for entry in "$PWD"/*; do
        i=$((i+1))
    done

    # Loop on the files in the current folder
    for entry in "$PWD"/*; do
        y=$((y+1))
        name="\"$(basename "$entry" | cut -f1 -d'.')\""
        if [ $y != $i ]
        then
            name="${name},"
        fi
        echo -e "\t\t${name}" >> "../${indexFile}"
    done
    if [ "${#folders[@]}" -eq $x ]
    then
        echo -e "\t]" >> "../${indexFile}"
    else
        echo -e "\t]," >> "../${indexFile}"
    fi
    x=$((x+1))
    cd .. || exit
done

echo "}" >> $indexFile