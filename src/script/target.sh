#!/bin/bash

cd "$PWD"/src/data || exit

targetFile="../js/target.js";

declare -a folders

i=1
for entry in "$PWD"/*; do
    folder="$(basename "$entry" | cut -f1 -d'.')"
    folders+=("$folder")
    ((i++))
done

if [ "${folders[*]}" = "*" ]
then
    echo "Veuillez ajouter un projet dans le répertoire 'src/data'"
    exit
elif [ "${#folders[@]}" -eq 1 ]
then
    response="0"
    echo "On prend le seul projet que vous avez"
else
    i=1
    echo "Quel projet voulez-vous lancer ?"
    for entry in "$PWD"/*; do
        echo "$i) ${folders[i-1]}"
        ((i++))
    done

    read response

    if [ response = null ]
    then
        response="1"
    fi
fi

truncate -s 0 $targetFile
echo "\"use strict\";" >> $targetFile
echo >> $targetFile
echo "export const target = \"${folders[response-1]}\";" >> $targetFile

cd "${folders[response-1]}"/data || exit

bash ../../../script/index.sh
bash ../../../script/tag.sh