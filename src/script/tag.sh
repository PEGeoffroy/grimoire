#!/bin/bash

tagFile="tag.json";

declare -a folders
declare -a tags
declare -a list
declare -a tagArray
declare -a urlArray

for entry in "$PWD"/*; do
    folder="$(basename "$entry" | cut -f1 -d'.')"
    if [ "$folder" != "index" -a "$folder" != "tag" ]
    then
        folders+=("$folder")
    fi
done

for folder in "${folders[@]}"
do
    for file in "$PWD/$folder"/*; do
        tag=$( jq .tag $file )
        if [ "$tag" != 'null' ]
        then
            len=$(echo ${tag} | jq '. | length')
            i=0
            x=1
            while [ "$i" -lt "$len" ]
            do
                word=$(echo $( jq .tag[$i] $file ))
                word=${word#"\""}
                word=${word%"\""}
                ((i++))
                element=$word.$folder/$(basename "$file" | cut -f1 -d'.')
                list+=($element)
            done
        fi
    done
done

list=($(echo ${list[*]}| tr " " "\n" | sort -n))

for element in "${list[@]}"
do
    name=$(echo $element | cut -f2- -d'/')
    url=$(echo $element | cut -f2- -d'.')
    letter=$(echo $url| cut -f1 -d'/')
    tag=$(echo $element | cut -f1 -d'.')
    tagArray+=($tag)
    urlArray+=($(echo ${letter:0:1}_$name))
done

# Empty the files content
truncate -s 0 $tagFile

echo "{" >> $tagFile

i=0
len=$(echo "${#tagArray[@]}")
((len--))

for tag in "${tagArray[@]}"
do
    if [ $i -eq 0 ]
    then
        if [ ${tagArray[i+1]} != ${tagArray[i]} ]
        then
            # different after
            echo -e "\t\"${tagArray[i]}\": [" >> $tagFile
            echo -e "\t\t\"${urlArray[i]}\"" >> $tagFile
            echo -e "\t]," >> $tagFile
        else
            # same after
            echo -e "\t\"${tagArray[i]}\": [" >> $tagFile
            echo -e "\t\t\"${urlArray[i]}\"," >> $tagFile
        fi
    elif [ $i -eq $len ]
    then
        if [ ${tagArray[i-1]} != ${tagArray[i]} ]
        then
            # different before
            echo -e "\t\"${tagArray[i]}\": [" >> $tagFile
            echo -e "\t\t\"${urlArray[i]}\"" >> $tagFile
            echo -e "\t]" >> $tagFile
        else
            # same before
            echo -e "\t\t\"${urlArray[i]}\"" >> $tagFile
            echo -e "\t]" >> $tagFile
        fi
    else
        if [ ${tagArray[i-1]} != ${tagArray[i]} ]
        then
            # different before
            echo -e "\t\"${tagArray[i]}\": [" >> $tagFile
        fi
        if [ ${tagArray[i+1]} != ${tagArray[i]} ]
        then
            # different after
            echo -e "\t\t\"${urlArray[i]}\"" >> $tagFile
            echo -e "\t]," >> $tagFile
        else
            # same after
            echo -e "\t\t\"${urlArray[i]}\"," >> $tagFile 
        fi
    fi
    ((i++))
done

echo "}" >> $tagFile