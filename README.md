# Readme

> Grimoire

Need :

* jq

```sh
sudo apt install jq
```

## Launch

To run the project:

```sh
npm install
npm run all
```

It will launch:

* A sass watcher
* A lite server

***

> You need to add data repository to data folder (don't worry, they will be ignored)

To do systematically for one of these reasons :

* tag addition
* new json file
* project change

```sh
npm run target
```
